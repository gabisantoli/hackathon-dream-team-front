import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  /*
  listaExemplo: string[];

  imageSoure: string = 'https://www.bambui.ifmg.edu.br/portal_padrao_joomla/joomla/images/phocagallery/galeria2/thumbs/phoca_thumb_l_image03_grd.png';
*/
  formulario: FormGroup;

  login: number;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router) {
    //this.listaExemplo = adminService.getLista();
  }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      email: ['',[Validators.required]],
      senha: ['',[Validators.required]]
    });
  }

  onSubmit() {
    this.login = null;
    let usuario = this.adminService.login(this.formulario.value);
    if (usuario == null){
      this.login = 0;
      return;
    }   
    this.router.navigate(['/posts']);
  }

}
