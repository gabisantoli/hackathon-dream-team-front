import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private loggedIn = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    localStorage.removeItem("loggedUser");
  }

  getLista() {
    return ['Java', 'angular', 'outro'];
  }

  login(data: any) {
    this.loggedIn.next(true);
    let usuario = {
      id: 1,
      nome: 'gabriela',
      login: 'gabi@test.com',
      senha: 'teste'
    };
    localStorage.setItem('loggedUser', JSON.stringify(usuario));
    return usuario;
    /*this.http.post('http://localhost:8080/usuario/login',
      data)
      .subscribe(
        resultado => {
          localStorage.setItem('loggedUser', JSON.stringify(resultado));
          this.loggedIn.next(true);
          return resultado;
        },
        erro => {
          console.log(erro)
          return null;
        }
      );*/
  }

  logout() {
    this.loggedIn.next(false);
    localStorage.removeItem("loggedUser");
    this.router.navigate(['/']);
  }
}
