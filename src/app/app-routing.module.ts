import { PostsComponent } from './posts/posts.component';
import { OrganizacaoComponent } from './organizacao/organizacao.component';
import { AdminComponent } from './admin/admin.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EngenheirosComponent } from './engenheiros/engenheiros.component';

const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'organizacao', component: OrganizacaoComponent },
  { path: 'engenheiros', component: EngenheirosComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
