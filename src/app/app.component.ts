import { AdminService } from './admin/admin.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    
  loggedUser$: Observable<boolean>;
  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    localStorage.removeItem("loggedUser");
    this.loggedUser$ = this.adminService.isLoggedIn;
  }

  logout(){
    this.adminService.logout();
  }

}
