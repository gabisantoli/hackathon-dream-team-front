import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngenheirosComponent } from './engenheiros.component';

describe('EngenheirosComponent', () => {
  let component: EngenheirosComponent;
  let fixture: ComponentFixture<EngenheirosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngenheirosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngenheirosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
