import { Component, OnInit } from '@angular/core';
import { EngenheirosService } from './engenheiros.service';

@Component({
  selector: 'app-engenheiros',
  templateUrl: './engenheiros.component.html',
  styleUrls: ['./engenheiros.component.css']
})
export class EngenheirosComponent implements OnInit {

  engenheiros: any;

  constructor(private engenheirosService: EngenheirosService) { }

  ngOnInit(): void {
    this.engenheiros = this.engenheirosService.getAll();
  }
}
