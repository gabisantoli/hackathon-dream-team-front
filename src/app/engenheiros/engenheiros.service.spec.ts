import { TestBed } from '@angular/core/testing';

import { EngenheirosService } from './engenheiros.service';

describe('EngenheirosService', () => {
  let service: EngenheirosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EngenheirosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
