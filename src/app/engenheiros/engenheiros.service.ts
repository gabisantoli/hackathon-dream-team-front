import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EngenheirosService {

  constructor(
    private http: HttpClient
  ) { }


  getAll() {
    let engenheiros = [
      {
        id: 1,
        nome: 'Exemplo',
        email: 'Exemplo',
        telefone: 'Exemplo',
        racf: 'Exemplo',
        cliente: 'Exemplo',
        squad: 'Exemplo',
        tecnologia: 'Exemplo',
      },
      {
        id: 1,
        nome: 'Exemplo',
        email: 'Exemplo',
        telefone: 'Exemplo',
        racf: 'Exemplo',
        cliente: 'Exemplo',
        squad: 'Exemplo',
        tecnologia: 'Exemplo',
      }
    ];
    return engenheiros;
    /*this.http.get('http://localhost:8080/engenheiro/view')
      .subscribe(
        resultado => {
          return resultado;
        },
        erro => {
          console.log(erro)
          return null;
        }
      );*/
  }
}
