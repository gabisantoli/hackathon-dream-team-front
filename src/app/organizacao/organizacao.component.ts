import { getTestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { OrganizacaoService } from './organizacao.service';

@Component({
  selector: 'app-organizacao',
  templateUrl: './organizacao.component.html',
  styleUrls: ['./organizacao.component.css'],
})
export class OrganizacaoComponent implements OnInit {

  organizacao: any;

 constructor(private organizacaoService: OrganizacaoService) { }

  ngOnInit(): void {
    this.organizacao = this.organizacaoService.getAll();
  }

}
