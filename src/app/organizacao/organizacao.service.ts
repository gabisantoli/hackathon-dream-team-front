import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrganizacaoService {

  constructor(
    private http: HttpClient
  ) { }


  getAll() {
    let organizacao = [
      {
        comunidade: 'Exemplo',
        rt: 'Exemplo',
        squad: 'Exemplo',
        cliente: 'Exemplo',
      },
      {
        comunidade: 'Exemplo',
        rt: 'Exemplo',
        squad: 'Exemplo',
        cliente: 'Exemplo',
      }
    ];
    return organizacao;
    /*this.http.get('http://localhost:8080/organizacaoView')
      .subscribe(
        resultado => {
          return resultado;
        },
        erro => {
          console.log(erro)
          return null;
        }
      );*/
  }
}
