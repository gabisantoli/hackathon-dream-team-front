import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private http: HttpClient
  ) { }

  getAll() {
    let posts = [
      {
        id: 1,
        usuarioNome: 'Gabriela',
        imagem: 'https://cdn.pixabay.com/photo/2021/04/02/12/35/zierpflaume-6144845__340.jpg',
        titulo: 'Titulo',
        subTitulo: 'SubTitulo',
        conteudo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        comentarios: [{
          id: 1,
          usuarioNome: 'Fernanda',
          conteudo: 'Lindas',
          post_id: 1
        }, {
          id: 2,
          usuarioNome: 'Fernanda',
          conteudo: 'Fofo',
          post_id: 1
        }],
      },
      {
        id: 2,
        usuarioNome: 'Gabriela',
        imagem: 'https://cdn.pixabay.com/photo/2021/04/03/02/38/kingfisher-6146356__340.jpg',
        titulo: 'Titulo',
        subTitulo: 'SubTitulo',
        conteudo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        comentarios: [{
          id: 3,
          usuarioNome: 'Fernanda',
          conteudo: 'Lindas',
          post_id: 2
        }, {
          id: 4,
          usuarioNome: 'Fernanda',
          conteudo: 'Fofo',
          post_id: 2
        }],
      }
    ];
    return posts;
    /*
    this.http.get('http://localhost:8080/post/view')
      .subscribe(
        resultado => {
          console.log(resultado);
          return resultado;
        },
        erro => {
          console.log(erro)
          return null;
        }
      );*/
  }
}
